# Patch Releases

Per the [Maintenance Policy], patch releases only include bug fixes for the current stable
released version of GitLab. Patches are released on an as-needed basis in order to fix regressions
in the current version. Non-security patches that are outside of our [Maintenance Policy] for bug
fixes must be requested and agreed upon by the Release Managers and the requester (see
[backporting to versions outside the Maintenance Policy] for details).

The timing of patch release is at the discretion of the [release managers]. Once the release managers
begin preparing a patch release they may make the decision to remove any lower severity MRs.
This is to avoid an S1 fix, or large number of other fixes from being delayed by a lower severity fix.

* [Overview]
* [Process for GitLab engineers]
* [Process for Release Managers]

## Overview

This section provides an overview of the end-to-end process for a patch release.

![patch release diagram](./images/patch_release_diagram.png)

[Source] - Internal link only

1. **Prepare** - A merge request backporting a bug fix to the current version is prepared by
   GitLab engineers.
1. **Test** - The merge request executes end-to-end tests via package-and-test pipeline
   to guarantee the bug fix meets the quality standards. If the package-and-test pipeline fails, a review from
   a Software Engineer in Test is required.
1. **Merge** - The merge request is merged by a GitLab maintainer in the stable branch associated
   with the current version.
1. **Analysis** - To determine if a patch release is required, release managers analyze the severity and number of backports waiting to be patch along other release pressures.
1. **Tag** - Release managers tag a new patch release package for the current version.
1. **Deploy** - The patch release package is deployed and tested to the GitLab Release instance.
1. **Release** - Release managers publish the packages associated with the patch release.

Details of steps 1 to 3 can be seen on the [Process for GitLab engineers] and details for steps 4 to 7
can be found on the [Process for Release Managers].

---

[Return to Guides](../README.md#guides)

[Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[backporting to versions outside the Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases
[overview]: #overview
[release managers]: https://about.gitlab.com/community/release-managers/
[Process for GitLab engineers]: engineers.md
[Process for Release Managers]: release_managers.md
[Source]: https://docs.google.com/presentation/d/1YRjA1dYCXNXp06VltDYlik1MdFyzUvaeXKk69mMPcA4/edit#slide=id.g226a611e9ec_0_0

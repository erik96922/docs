# Security Releases general information

Security vulnerabilities in GitLab and its dependencies are to be addressed with
the highest priority.

Security releases are naturally very similar to [patch releases](../patch/process.md), but
on a much shorter timeline. The goal is to make a security release available as
soon as possible, while ensuring that the security issue is properly addressed
and that the fix does not introduce regressions.

At GitLab, we have two types of security releases:

- [Scheduled](#scheduled-security-releases): A planned security release to publish all available vulnerability fixes to
    the [current, and previous two GitLab versions]. Scheduled security releases normally take place after the [monthly release].
- [Critical](#critical-security-releases): An unplanned, immediate patch and mitigation is required for a single issue.

Also see [the GitLab Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html)
for the information on determining supported releases and assigning versions.

## Guides by role

- [Release Manager]
- [Security Engineer]
- [Developer]
- [Quality Engineer]

### Terminology

1. **Security vulnerability issue** - A confidential issue in a GitLab Canonical project (`gitlab-org/some-project`), detailing a security vulnerability and steps to reproduce.

   Usually created via HackerOne.

   Example: [gitlab-org/gitlab#193100](https://gitlab.com/gitlab-org/gitlab/issues/193100).

1. **Security release tracking issue** - A confidential issue in `gitlab-org/gitlab` which is the high-level overview of an entire security release (critical or otherwise).

   Created via [issue template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20Release.md).

   Example: [gitlab-org/gitlab#209019](https://gitlab.com/gitlab-org/gitlab/-/issues/209019).

1. **Security implementation issue** - An issue in a GitLab Security project (`gitlab-org/security/some-project`), outlining the versions affected, detailing the steps a developer needs to take to remediate the vulnerability, and associating backports.

   Created via [issue template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md).

   Example: [gitlab-org/security/gitlab#80](https://gitlab.com/gitlab-org/security/gitlab/-/issues/80).

1. **Security merge request (MR)** - A merge request in a GitLab Security project (`gitlab-org/security/some-project`), resolving a vulnerability in a specific branch.

   Created via [merge request template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Security%20Release.md).

   Example: [gitlab-org/security/gitlab!314](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/314).

1. **Release task issue** - Used by [release managers] as a checklist of steps to complete the release.

   Created via ChatOps (`/chatops run release prepare --security`).

   Example: [gitlab-org/release/tasks#1272](https://gitlab.com/gitlab-org/release/tasks/-/issues/1272).

#### Issue association

```mermaid
graph RL
  classDef canonical fill:#74bd3d
  classDef security fill:#ff7272

  secVuln(Security vulnerability Issue)
  secRelease(Security release Issue)
  secFix(Security implementation issue)
  task{{Release task issue}}

  class secVuln canonical
  class secRelease canonical
  class secFix security

  secFix -->|references| secVuln
  secFix ---|related| secRelease

  task -.->|references| secRelease
```

#### Merge request association

```mermaid
graph RL
  classDef canonical fill:#74bd3d
  classDef security fill:#ff7272

  secFix(Security implementation issue)
  secMR1(["Security MR 1 (default-branch)"])
  secMR2(["Security MR 2 (backport)"])
  secMR3(["Security MR 3 (backport)"])
  secMR4(["Security MR 4 (backport)"])

  class secFix security
  class secMR1,secMR2,secMR3,secMR4 security

  secMR1 & secMR2 & secMR3 & secMR4 -.->|references| secFix
```
#### Merging

```mermaid
graph TD
  classDef security fill:#ff7272

  coMerge{{"/chatops run release merge --security"}}
  coMergeDefaultBranch{{"/chatops run release merge --security --default-branch"}}

  secMR1(["Security MR 1 (default-branch)"])
  secMR2(["Security MR 2 (backport)"])
  secMR3(["Security MR 3 (backport)"])
  secMR4(["Security MR 4 (backport)"])

  autodeploy(auto-deploy)
  defaultbranch(default-branch)

  stable129(12-9-stable-ee)
  stable128(12-8-stable-ee)
  stable127(12-7-stable-ee)

  coMerge --> secMR2
  coMerge --> secMR3
  coMerge --> secMR4

  secMR2 --> |merge| stable129
  secMR3 --> |merge| stable128
  secMR4 --> |merge| stable127

  coMergeDefaultBranch --> secMR1
  secMR1 --> |merge| defaultbranch
  defaultbranch -.-> |cherry-pick| autodeploy

  class secMR1,secMR2,secMR3,secMR4 security
  class stable129,stable128,stable127 security
  class defaultbranch,autodeploy security
```

[release managers]: https://about.gitlab.com/community/release-managers/

## Scheduled Security Releases

After the [monthly release], we release patch versions of GitLab containing
non-critical security fixes. This date has been chosen as it gives enough
time to release a non-security patch release after standard release day.

### Security Release deadlines

Just as with patch releases, deadlines for planned security releases are at
the discretion of release managers, a week after the [monthly release] is used as default due date.

Final deadline will depend on a variety of factors:
* GitLab.com availability.
* Workload on Release Managers.
* Stability of auto-deployments.

The [Security Release Tracking Issue] is the single source of truth (SSOT) for what is included in the security release. Only issues that are linked to the Security Release Tracking Issue are guaranteed to be in the release. In most cases, this linking is managed by release tools automation.

In the run-up to the due date, the release tooling will be evaluating any
security implementation issues containing the ~"security-target" label. The security implementation issues will automatically be linked to the current Security Release Tracking Issue if they are ready. If they are not ready, comments will be posted on the security implementation issue directing the assignee on what actions still need to be taken.

To guarantee the security fix is linked to the Security Release Tracking Issue, **the ~"security-target" label should be applied to the security implementation issue with all associated merge requests
ready to be evaluated and processed 48h (or 2 working days if occuring over a weekend or other non-working day) before the due date** established in the
Security Release Tracking Issue.

Fixes not linked by 2 working days before the due date may not be considered for the security release
and they'll have to go into the next one.

If you have an urgent security issue that cannot wait for the next security release, you can ping
the [Release Managers](https://about.gitlab.com/community/release-managers/) for an exception.

## Critical Security Releases

> **:warning: NOTE:** Starting with the 16.7 milestone, the Delivery team is going to experiment with an additional planned security release as an effort to [adopt two security releases per month]. 
> This effort will allow us to halve the cycle time of security releases and help deliver security improvements whilst meeting [remediation SLAs].
> During this experiment, **if a high-severity security fix is discovered, please proceed to include them in the [next planned release]**. The scheduled releases guarantee
> bug and security fixes are released at least every 23 days to meet bug and security SLAs.
>
> The relevance of the [unplanned critical security release process is being analyzed for deprecation](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19904)

Depending on the severity and impact of the vulnerability, an
immediate patch release consisting of just the security fix may be warranted.

The critical release process is a superset of a non-critical release, and for
completeness the process is written here:

1. The vulnerability has been reported and discussed between a [Security
   Engineer] and a [Developer]. When the timeline of a possible fix is
   established, the Security Engineer informs a [Release Manager] of a need
   for the critical security release.
1. A release manager proposes a timeline for the critical security release based
   on the other release tasks, and informs a security engineer and a [Quality
   Engineer] in the designated release issue.
1. Depending on the nature of the issue, a security engineer may decide that
   a temporary mitigation strategy is required. If a patch is required,
   the security engineer works with the developer and infrastructure engineer
   following the [post-deployment patch process](../deploy/post-deployment-patches.md).
   In other cases, the security engineer will work with infrastructure
   engineers on remediation.
1. The security engineer creates a [security operations issue](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/new?issuable_template=User+Impact+Investigation)
   using the template `user_impact_investigation` for the
   security operations team to track further investigations and other post-remediation
   activities, such as determining and notifying affected users.
1. A security engineer works on a blog post to pre-announce the critical
   security release. They also work with the marketing team to inform the
   customers of an upcoming release. They also follow the [JiHu Support Security Release Process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/#security-release-process).
   The security engineer also prepares the second blog post which will be published
   at release time.
1. The developer follows the [Developer process](developer.md) to create the
   artifacts necessary for a release.
1. A release manager prepares the release with all backports, and deploys to
   staging (if applicable). If there are any non security releases pending, such
   as a release candidate, release managers may decide to release those first.
1. If any post-deployment patches were applied, an on-call Security engineer
   should verify that the applied patches are still working; unless the newly
   deployed version removes the need for these patches.
1. A quality engineer prepares environments and executes QA tasks together with
   the security engineer to verify that all released versions have successfully
   resolved the vulnerability.
1. When all environments contain the fix, any temporary mitigation strategy is
   reverted.
1. Following deployment, a release manager coordinates with a security engineer
   on the exact timing of a release.
1. A release manager will promote the packages at the designated time, and merge
   the release blog post.
1. A security engineer works with the marketing team to send notification emails
   to any affected users.
1. A release manager closes all release issues.
1. A security engineer keeps the issues where the vulnerabilities were reported
   open for the next 30 days.
1. A security engineer prepares a blog post that explains the vulnerability in
   detail and releases it approximately 30 days after the original release.
1. Once the final blog post is released, a security engineer removes the
   confidentiality from the issues and closes them.

Each involved role should follow their own guide, and create separate issues
linking to the main release issue.

---

[Return to Guides](../README.md)

[Release Manager]: release-manager.md
[Security Engineer]: security-engineer.md
[Developer]: developer.md
[Quality Engineer]: quality.md
[Security Release Tracking Issue]: https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=upcoming%20security%20release
[monthly release]: https://about.gitlab.com/releases/
[current, and previous two GitLab versions]: https://docs.gitlab.com/ee/policy/maintenance.html
[adopt two security releases per month]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1125
[remediation SLAs]: https://handbook.gitlab.com/handbook/security/threat-management/vulnerability-management/#remediation-slas
[next planned release]: https://gitlab.com/gitlab-org/gitlab/-/issues/?label_name%5B%5D=upcoming%20security%20release
